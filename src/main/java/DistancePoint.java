public class DistancePoint {
    private double dist;
    PointList coord = new PointList();

    public DistancePoint(double v, double v1, double v2, double v3) {
    }

    public DistancePoint() {

    }

    public  double dist(double pointX, double pointY, double circlePointX, double circlePointY) {

        dist = Math.sqrt((pointX - circlePointX) * (pointX - circlePointX) + (pointY - circlePointY) * (pointY - circlePointY));
        //System.out.println("Расстояние между точками="+dist);
        return dist;
    }
    public void setDist(double pointX, double pointY, double circlePointX, double circlePointY) {
        this.dist=Math.sqrt((pointX - circlePointX) * (pointX - circlePointX) + (pointY - circlePointY) * (pointY - circlePointY));;
    }
    public double getDist() {
        return dist;
    }
}
